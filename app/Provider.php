<?php

namespace App;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;


class Provider implements CarrierInterface
{
    public function dialContact(Contact $contact)
    {
        echo "Dialing the phone ".$contact->phone." of the contact ".$contact->name;
    }

    public function makeCall(Contact $contact): Call
    {
        $call = new Call($contact->phone);
        echo "- Calling to ".$call->phone;

        return $call;
    }
}
