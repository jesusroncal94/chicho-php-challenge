<?php

namespace App\Services;

use App\Contact;
use phpDocumentor\Reflection\Types\String_;

class ContactService
{
	
	public static function findByName($name): Contact
	{
		// queries to the db
		$contacts = array(
			array(
				"uid"	 => "156",
				"name"	 => "Jesus",
				"phone" => "123456789"
			),
			array(
				"uid"	 => "65412",
				"name"	 => "Enrique",
				"phone" => "987654321"
			)
		);

		$found_value = array_search($name, array_column($contacts, 'name'));
		
        if($found_value !== FALSE)
		{
			$contact = new Contact(
				$contacts[$found_value]["uid"],
				$contacts[$found_value]["name"],
				$contacts[$found_value]["phone"]
			);
			return $contact;
		} else
		{
			return new Contact();
		}
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
		return True;
	}
}