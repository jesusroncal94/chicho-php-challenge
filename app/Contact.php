<?php

namespace App;


class Contact
{
	
	function __construct($uid = null, $name = null, $phone = null)
	{
		$this->uid = $uid;
		$this->name = $name;
		$this->phone = $phone;
	}
}