<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;

use App\Call;
use App\Mobile;
use App\Provider;


class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new Provider();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function test_makeCallByName()
	{
		$provider = new Provider();
		$mobile = new Mobile($provider);

		# $this->assertIsObject($mobile->makeCallByName('Jesus'));
		$this->assertInstanceOf(Call::class, $mobile->makeCallByName('Jesus'));
	}
}
